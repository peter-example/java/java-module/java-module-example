module hk.quantr.modulea {
	exports hk.quantr.modulea;
	requires hk.quantr.modulebase;
	provides hk.quantr.modulebase.IGreeting with hk.quantr.modulea.ModuleA;
}
