package hk.quantr.modulea;

import hk.quantr.modulebase.IGreeting;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ModuleA implements IGreeting {

	@Override
	public String regular(String str) {
		return str + " " + str;
	}

}
