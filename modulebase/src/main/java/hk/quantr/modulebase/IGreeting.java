package hk.quantr.modulebase;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public interface IGreeting {

	public String regular(String str);
}
