package hk.quantr.moduleb;

import hk.quantr.modulebase.IGreeting;
import java.io.IOException;
import java.util.Iterator;
import java.util.ServiceLoader;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MainApp {

	public static void main(String[] args) throws IOException, Exception {
		Class<IGreeting> personClass = IGreeting.class;
		Module module = personClass.getModule();
		System.out.println(module.getName());

		ServiceLoader<IGreeting> sl = ServiceLoader.load(IGreeting.class);
		Iterator<IGreeting> apit = sl.iterator();
		while (apit.hasNext()) {
			System.out.println(apit.next().regular("pp"));
		}
	}
}
